<?php
$teams = ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'B10'];
$pairs = [];
$usedPairs = [];
$chekLstMatch = [];
function push($element)
{
    global $chekLstMatch;
    // If the array has reached the maximum size, remove the first element
    if (count($chekLstMatch) >= 4) {
        array_shift($chekLstMatch);
    }
    // Add the new element to the end of the array
    $chekLstMatch[] = $element;
}
$currentDate = strtotime('2023-10-01');
// for get first matches 
for ($i = 0; $i < count($teams); $i += 2) {
    push($teams[$i]);
    push($teams[$i + 1]);
    $pair = $teams[$i] . " vs " . $teams[$i + 1];
    $usedPairs[] = $pair;
    $pairs[] = [
        "round" => 1,
        "Team" => $pair,
        "date" => date('d-m-Y', $currentDate),
    ];
    // echo "Date:" . date('d-m-Y', $currentDate) . "- Match - " . $pair;
    // Increment the date after adding it to the pairs array
    $currentDate += 86400;
}
for ($a = 0; $a < count($teams); $a++) {
    for ($i = 0; $i < count($teams); $i++) {
        for ($j = $i + 1; $j < count($teams); $j++) { // Start from $i + 1 to avoid self-pairing
            if (!in_array($teams[$i], $chekLstMatch) && !in_array($teams[$j], $chekLstMatch)) {
                $pair = $teams[$i] . " vs " . $teams[$j];
                if (!in_array($pair, $usedPairs)) {
                    push($teams[$i]);
                    push($teams[$j]);
                    $usedPairs[] = $pair; // Track used pairs
                    $pairs[] = [
                        "round" => 2,
                        "Team" => $pair,
                        "date" => date('d-m-Y', $currentDate),
                    ];
                    // echo "Date:" . date('d-m-Y', $currentDate) . "- Match - " . $pair;
                    // Increment the date after adding it to the pairs array
                    $currentDate += 86400;
                }
            }
        }
    }
}
$matches = []; // for keep all matches
for ($i = 0; $i <= count($teams); $i++) {
    for ($j = $i + 1; $j < count($teams); $j++) {
        $team1 = $teams[$i];
        $team2 = $teams[$j];
        $matches[] = $team1 . ' vs ' . $team2;
    }
}
// add missing matches 
shuffle($matches);
foreach ($matches as $item) {
    if (!in_array($item, $usedPairs)) {
        $usedPairs[] = $item; // Track used pairs
        $pairs[] = [
            "round" => 2,
            "Team" => $item,
            "date" => date('d-m-Y', $currentDate),
        ];
        // echo "Date:" . date('d-m-Y', $currentDate) . "- Match - " . $pair;
        // Increment the date after adding it to the pairs array
        $currentDate += 86400;
    }
}
$currentDate += 86400 * 3;
// Schedule semi-final matches
$semiFinalTeams = array_slice($teams, 0, 4); // Assuming the top 4 teams qualify for the semi-finals
//first semi-final
$pairs[] = [
    "round" => 3,
    "Team" => $semiFinalTeams[0] . " vs " . $semiFinalTeams[3],
    "date" => date('d-m-Y', $currentDate),
];
$currentDate += 86400 * 3;

$pairs[] = [
    "round" => 4,
    "Team" => $semiFinalTeams[1] . " vs " . $semiFinalTeams[2],
    "date" => date('d-m-Y', $currentDate),
];
$currentDate += 86400 * 3;

// Schedule the final match

$pairs[] = [
    "round" => 5,
    "Team" => "Winner SF1" . " vs " . "Winner SF2",
    "date" => date('d-m-Y', $currentDate),
];
//print the list
echo "Round Match <br />";
foreach ($pairs as $key => $value) {
    echo $value['round'] == 3 ? "First Semi-Final <br />" : "";
    echo $value['round'] == 4 ? "Second Semi-Final <br />" : "";
    echo $value['round'] == 5 ? "Final Match <br />" : "";
    echo "Date:" . $value['date'] . "- Match - " . $value['Team'] . "<br />";
}
// echo "<pre>";
// echo json_encode($pairs);


// first round last 4 match have some probelem its not a proper dates